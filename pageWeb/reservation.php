<html>
   <head>
      <title>Signup</title>
   </head>
   <body>
	  <h1>Toutes les réservations</h1>
          <?php
                // Connexion a la base de donnees
                $base='db_hotel.sqlite';
                $db= new PDO('sqlite:/home/tafonso/Bureau/mqtt-bureau/db_hotel','','',array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                // On resupere toutes les reservations de la base de donnees
                $sql='SELECT date_debut,date_fin,Nb_personne, Numero_chambre,Nom FROM reservation';
	  ?>
	        <table style="border-collapse:collapse;">
                	<tr>
                        	<th style="border: 1px solid black;">Date début</th>
                        	<th style="border: 1px solid black;">Date fin</th>
                        	<th style="border: 1px solid black;">Nombre de personnes</th>
                        	<th style="border: 1px solid black;">Numéro de chambre</th>
                        	<th style="border: 1px solid black;">Nom</th>
                	</tr>
	  		<?php
				foreach($db->query($sql) as $row){
                        	$dateD= $row['date_debut'];
                        	$dateF= $row['date_fin'];
                        	$num=$row['Numero_chambre'];
                        	$nbPers= $row['Nb_personne'];
                        	$nom=$row['Nom'];
	  		?>
	  		<tr>
	  			<td style="border: 1px solid black;"><?php echo $dateD ?> </td>
	  			<td style="border: 1px solid black;"><?php echo $dateF ?> </td>
          			<td style="border: 1px solid black;"><?php echo $nbPers ?> </td>
          			<td style="border: 1px solid black;"><?php echo $num ?> </td>
          			<td style="border: 1px solid black;"><?php echo $nom ?> </td>	
          		</tr>
          		<?php  } ?>
          	</table>
   </body>
</html>
