

<html>
   <head>
      <title>Formulaire</title>
		<?php

		/*************  Connexion a la base de donnees  **************/

                $base='db_hotel.sqlite';
                $db= new PDO('sqlite:/home/tafonso/Bureau/mqtt-bureau/db_hotel','','',array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		

		/*************  On recupere toutes les reservations de la base de donnees (date debut - date fin - numero de chambre)  ***************/

		$tab=[];// Tableau contenant toutes les reservations (ex: [[08/07/2020,20/07/2020,102],[22/08/2020,24/08/2020,204]...]
		$tabUneRes=[];// Tableau contenant une seule reservation. Permet de remplir le tableau $tab
		//On recupere toutes les reservations et on les met dans le tableau $tab
		$sql='SELECT date_debut,date_fin, Numero_chambre FROM reservation';
                foreach($db->query($sql) as $row){
			$dateD= $row['date_debut'];
			$dateF= $row['date_fin'];
			$num=$row['Numero_chambre'];
			array_push($tabUneRes,$dateD,$dateF,$num);
			array_push($tab,$tabUneRes);
			$tabUneRes=[];
		}
		
                /*************  On recupere toutes les chambres de la base de donnees (numero de chambre - place)  ***************/

		$tabUneChambre=[];// Tableau contenant une seule chambre. Permet de remplir le tableau $tabChambres
		$tabChambres=[];// Tableau contenant toutes les chambres (ex: [[102,3],[103-1]...]
		//On recupere toutes les chambres et on les met dans le tableau $tabChambres
		$sql2='SELECT numero,place FROM chambre';
                foreach($db->query($sql2) as $row2){
                        $unNum= $row2['numero'];
                        $nbPlace= $row2['place'];
                        array_push($tabUneChambre,$unNum,$nbPlace);
                        array_push($tabChambres,$tabUneChambre);
                        $tabUneChambre=[];
                }
		?>

		<script type="text/javascript">
		
		/**********************  Validation du formulaire     **********************/

		// Le formulaire n'est pas envoye si il n'y a pas assez de chambre ou de place disponible pour la date, le nombre de personne et le nombre de chambre demande
		function validate(){
			// On recupere les reponse du formulaire
			var nbPers=document.getElementById("nbPersonne").value;
			var nbChambre=document.getElementById("nbChambre").value;
			var dateD=document.getElementById("dateD").value;
                        var dateF=document.getElementById("dateF").value;
 			var dd=new Date(dateD);
			var df=new Date(dateF);


    			var chambreTrouve=0;//Doit, a la fin, correspondre au nombre de chambre demande (nbChambre)
    			var placeTrouve=0; //Doit, a la fin, correspondre au nombre de personne demande (nbPers)
			var listChambre=[];//Liste des chambres permettant de répondre a la reservation
			var listPlace=[]; // Liste de toutes les place des chambres de la liste listChambres
			var x=0;// Si x=0 le formulaire est envoye

			//On recupere le tableau des chambres et des reservations de la base de donnees	
			var tabRes=<?php echo json_encode($tab) ?>;
			var tabC=<?php echo json_encode($tabChambres) ?>;

                     	//Permet de reformater les dates de la base de donnees
                        var pattern=/(\d{2})\/(\d{2})\/(\d{4})/;

			/************  Détermine les chambres pour le nombre de place et de chambre demandé ***********/
			
			//On parcour les chambres de la base de données
			for(var j=0;j<tabC.length;j++){
				//Si le nombre de place et de chambre demandé n'a pas été atteind ...
		    		if(parseInt(tabC[j][1],10)+placeTrouve<=parseInt(nbPers,10) && chambreTrouve<=parseInt(nbChambre,10)){
					//Si il ne reste plus qu'une chambre avant d'avoir atteind le nombre de chambre demandé ...
					if(chambreTrouve+1==parseInt(nbChambre,10)){
						//On ajoute la chambre a la liste "listChambres" si son nombre de place ateind le nombre de place demandé
						if(placeTrouve+parseInt(tabC[j][1],10)==parseInt(nbPers,10)){
							listChambre.push(tabC[j][0]);//On ajoute le numero de la chambre
                                                        listPlace.push(tabC[j][1]);//On ajoute le nombre de  place de la chambre
                                                        placeTrouve+=parseInt(tabC[j][1],10);//On ajoute le nombre de place de la chambre a placeTrouve
                                                        chambreTrouve+=1;// On ajoute 1 a chambreTrouve
							//On parcours les reservation de la base de donnees.Si la chambre selectionne avant est deja dans la base de donne et que ses date de reservation sont les memes que ceux que l'on souhaite, on retire la chambre de la liste. 
							for(var i=0;i<tabRes.length;i++){
                                				var dateD_db=new Date((tabRes[i][0]).replace(pattern,'$3-$2-$1'));
                                				var dateF_db=new Date((tabRes[i][1]).replace(pattern,'$3-$2-$1'));
                                				if(dateD_db.getTime()<=dd.getTime() && dateF_db.getTime()>=dd.getTime() || dateF_db.getTime()>=df.getTime() && dateD_db.getTime()<=df.getTime()){
										if(tabC[j][0]==tabRes[i][2]){

											listChambre.pop();//On retire la chambre
											listPlace.pop();//On retire la place de la chambre
											placeTrouve-=parseInt(tabC[j][1],10);//On retire le nombre de place de la chambre a placeTrouve
											chambreTrouve-=1;//On retire 1 a chambreTrouve
                                                				}
                                				}       
                        				}
						}
					//Si il reste plus d'une chambre avant d'atteindre le nombre de chambre demandé	
					}else{
						//On ajoute la chambre si son nombre de place est inférieur au nombre de palce demandé et inférieur au nombre de place limite pour ajouter le nombre de chambre demandé avec une place maximum de 1(ex: si on veut 2 chambres pour 4 personnes, le nombre de place de la chambre doit etre au maximum 3) 
						if(placeTrouve+parseInt(tabC[j][1],10)!=parseInt(nbPers,10) && parseInt(tabC[j][1],10)+parseInt(nbChambre,10)-1<=parseInt(nbPers,10)){
							listChambre.push(tabC[j][0]);
							listPlace.push(tabC[j][1]);
							placeTrouve+=parseInt(tabC[j][1],10);
							chambreTrouve+=1;
                                                        for(var i=0;i<tabRes.length;i++){
                                                               var dateD_db=new Date((tabRes[i][0]).replace(pattern,'$3-$2-$1'));
                                                               var dateF_db=new Date((tabRes[i][1]).replace(pattern,'$3-$2-$1'));
                                                                if(dateD_db.getTime()<=dd.getTime() && dateF_db.getTime()>=dd.getTime() || dateF_db.getTime()>=df.getTime() && dateD_db.getTime()<=df.getTime()){
			
										if(tabC[j][0]==tabRes[i][2]){
                                                                                        listChambre.pop();
                                                                                        listPlace.pop();
                                                                                        placeTrouve-=parseInt(tabC[j][1],10);
                                                                                        chambreTrouve-=1;
                                                                                }
                                                                }       
                                                        }

						}
					}
				}
			}

			//Calcul du nombre total de place trouvé
			var total=0;
			for(var y=0;y<listPlace.length;y++){
				total+=parseInt(listPlace[y],10);
			}
			
			//Si l'on a pas trouve assez de place pour le nombre de personne demande x=1 et le formulaire n'est pas envoye
			if(total<parseInt(nbPers,10)){
				x=1;
			}
			var str="";
                        if(x==1){
                                return false;
			}else{
				for(var i=0;i<listChambre.length;i++){
					str+="-"+listChambre[i];	
				}
				// Si le formulaire est valide on l'envoie. Le formulaire possede un champs invisible permettant d'envoyer les chambres selectionne (en chaine de charactere. ex: "-102-204" )
				var strNum=document.getElementById("numChambre").value=str;
			}
			return true;
	    	}
      </script>

   </head>

   <body>
	<a href="reservation.php" >Voir toutes les réservations</a>
	<h1>Formulaire de réservation</h1>
	<form name="formulaire" onsubmit="return(validate())" action="http://localhost:5000/sendForm" method="POST">
            <p>
               Votre nom :<br />
               <input type="text" name="nom" />
            </p>
            <p>
               Nombre de personne :<br />
	       <input type="number" id="nbPersonne" name="nbPersonne" />
            </p>
            <p>
               Nombre de chambre :<br />
               <input type="number" id="nbChambre"  name="nbChambre" />
            </p>
            <p>
               Date de début :<br />
               <input type="date" id="dateD" name="dateD" />
            </p>
            <p>
               Date de Fin :<br />
               <input type="date" id="dateF" name="dateF" />
	    </p>
	    <p>
               <input type="hidden" id="numChambre" name="numChambre" />
            </p>
            <input type="submit" name="submit"  value="Envoyer" />
         </form>
   </body>
</html>

