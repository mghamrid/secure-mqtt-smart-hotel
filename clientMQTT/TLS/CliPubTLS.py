import paho.mqtt.client as paho
import time
from cryptography.fernet import Fernet
from flask import Flask
from flask import request, redirect, render_template
from datetime import datetime

app = Flask(__name__)

broker="tafonso"
port="8883"

conn_flag=False
loop_flag=False

################   callback    #####################

#Appele lorsque le courtier repond a notre demande de connexion
def on_connect(client,userdata,flags,rc):
        global conn_flag
        if rc==0:
            conn_flag=True
            print("right username and password")
        if rc==5:
            print("setting wrong username or password")
        print("connected",conn_flag)
        conn_flag=True

def on_log(client,userdata,level,buf):
        print(buf)

#Appele lorsque le courtier repond a notre demande de deconnexion 
def on_disconnect(client,userdata,rc):
        print("client disconnected ok")

#Appele lorsque le courtier repond a notre publication
def on_publish(client,userdata,mid):
        global loop_flag
        print("acknowledgement received")
        loop_flag=True

#Permet de publier un message
def pub(client,topic,msg,qos):
    print("\n=========================  Publications  ========================\n")
    (rc,mid)=client.publish(topic,msg, qos)
    if rc==0:
        print("publish success, data :", msg)
    time.sleep(2)
    print('\n')

    while not loop_flag:
        time.sleep(1)
        print("waiting for acknowledgement...")

#Permet de connecter le client au broker
def connect(client,broker,port):
    print("\n=========================  Connection  ==========================\n")
    client.connect(broker,port)
    client.loop_start()
    while not conn_flag:
        time.sleep(1)
        print("waiting...")
    time.sleep(2)

#Creation du client
client=paho.Client("clientPub",False)
client.on_log=on_log

#Utilisation de TLS et de l authentification par mot de passe pour la connexion au broker
client.username_pw_set(username="user",password="1234")
client.tls_set('/home/tafonso/paho-mqtt/paho.mqtt.python/certs/ca.crt')

#On utilise les callback
client.on_connect=on_connect
client.on_publish=on_publish
client.on_disconnect=on_disconnect

#Connexion au broker
connect(client,broker,port)

###### Definition des routes #########

#Formulaire 
@app.route('/')
def formulaire():
    return  redirect('http://localhost/formul.php')

#Si le formualaire est soumis, cette route est utilisee
@app.route('/sendForm',methods=['POST'])
def sendFrom():
    
    conn_flag=False
    loop_flag=False
    
    #On recupere les donnees du formulaire
    nom=request.form['nom']
    nbPersonne=request.form['nbPersonne']
    nbChambre=request.form['nbChambre']
    dateD=request.form['dateD']
    dateF=request.form['dateF']
    numChambreStr=request.form['numChambre']
    dateDD=datetime.strptime(dateD,'%Y-%m-%d')
    dateFF=datetime.strptime(dateF,'%Y-%m-%d')

    #On defini le message a publier
    messageReservation=dateDD.strftime("%d/%m/%Y")+"-"+dateFF.strftime("%d/%m/%Y")+"-"+nbPersonne+"-"+nbChambre+"-"+nom+numChambreStr+"-"   
    print(messageReservation)
    
    #On publie le message
    pub(client,"hotel/reservation",messageReservation,2)
    
    #On redirige vers le formulaire
    return  redirect('http://localhost/formul.php')


if __name__=='__main__':
    app.run()
