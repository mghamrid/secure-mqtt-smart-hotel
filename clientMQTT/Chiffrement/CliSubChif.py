import time
import paho.mqtt.client as paho
import sqlite3
from cryptography.fernet import Fernet

broker="tafonso"
port="1883"
conn_flag=False
loop_flag=False

debut="not exist"
fin="not exist"
nom="not exist"
chambre="not exist"

################   callback    #####################

#Appele lorsque le courtier repond a notre demande de connexion
def on_connect(client,userdata,flags,rc):
        global conn_flag
        if rc==0:
            conn_flag=True
            print("right username and password")
        if rc==5:
            print("setting wrong username or password")
        print("connected",conn_flag)
        conn_flag=True

def on_log(client, userdata, level, buf):
    print(buf)

#Appele lorsque le courtier envoie un message
def on_message(client, userdata, message):
    #Dechiffrement du message
    cipher_key ='3LKEj4hGJH6uZKQHUKiMvzPA9Az2pj_xMyX_3QgZG2Q='
    cipher = Fernet(cipher_key)
    print("\n=========================  Message received  ====================\n")
    #Affichage du message chiffre recu
    print("receive payload : ",message.payload,"on topic: ",message.topic  )
    print('\n')
    decrypted_message = cipher.decrypt(message.payload)
    mess=str(decrypted_message.decode("utf-8"))
    #Affichage du message dechiffre recu
    print("received message =",str(decrypted_message.decode("utf-8")))
    print('\n')
    print("\n-----------------------------------------------------------------\n")

    
    #On decoupe la chaine de charactere recu pour obtenir la date de debut, la date de fin, le nombres de personne, les numeros de chambre et le nom du client
    #La date de debut, la date de fin, le nombres de personne on une taille fixe 
    debut=mess[0:10]
    fin=mess[11:21]
    personne=int(mess[22])
    #Le nom du client et les chambres on une taille qui varie selon les reservations. On utilise le charactere - pour les trouves
    messNom=mess[26:]
    index1=messNom.find("-")
    nom=messNom[:index1]

    chambresStr=messNom[index1+1:]
    tabC=[]
    premierIndex=0
    dernierIndex=0
    for unChar in chambresStr:
        if unChar=="-":
            dernierIndex=dernierIndex+3
            c=chambresStr[premierIndex:dernierIndex]
            tabC.append(c)
            premierIndex= dernierIndex+1
            dernierIndex=dernierIndex+1

    #Enregistrement de la reservation dans la base de donnees
    for numChambre in tabC:
                cursor.execute(""" INSERT INTO reservation(date_debut,date_fin,Nb_personne,Numero_chambre,Nom) VALUES(?,?,?,?,?) """,(debut,fin,personne,numChambre,nom))
                conn.commit()
    print("successful booking")

#Appele lorsque le courtier repond a notre demande de deconnexion 
def on_disconnect(client,userdata,rc):
        print("client disconnected ok")

#Appele lorsque le courtier repond a notre demande d abonnement
def on_subscribe(client, userdata, mid, granted_qos):
    global loop_flag
    loop_flag=True
   
#Permet de connecter le client au broker   
def connect(client,broker,port):
    print("\n=========================  Connection  ==========================\n")
    client.connect(broker,port)
    time.sleep(2)
    while not conn_flag:
        time.sleep(1)
        print("waiting...")
        client.loop()

#Permet d abonner le client a un topic
def sub(client,topic,qos):
    global loop_flag
    print("\n=========================  Subscription  ========================\n")
    (rc,mid)=client.subscribe(topic,qos)#subscribe
    while not loop_flag:
        time.sleep(1)
        print("waiting for callback")
        client.loop()
    if rc==0:
        print("Successful subscription")
    else:
        print("Subscription failed")
    loop_flag=False
    time.sleep(2)

#Connexion du client MQTT a la base de donnees
conn = sqlite3.connect('/home/tafonso/Bureau/mqtt-bureau/db_hotel')
cursor=conn.cursor()
cursor.execute("""CREATE TABLE IF NOT EXISTS reservation(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,date_debut TEXT,date_fin TEXT,Nb_personne INTEGER,Numero_chambre INTEGER,Nom TEXT)""")
conn.commit()

#Creation du client
client= paho.Client("clientSub",False)  
client.on_log=on_log

#Utilisation de l authentification par mot de passe pour la connexion au broker
client.username_pw_set(username="user2",password="5678")

#On utilise les callback
client.on_connect=on_connect
client.on_message=on_message
client.on_subscribe=on_subscribe
client.on_disconnect=on_disconnect

#Connexion au broker
connect(client,broker,port)
#Abonnement au topic "hotel/reservation"
sub(client,"hotel/reservation",2)

#En attente d une publication sur le topic "hotel/reservation"
print("\n  Waiting for publication ...  \n")
client.loop_forever()

conn.close()
client.disconnect()
