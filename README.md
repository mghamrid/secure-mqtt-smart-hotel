# Lancement du projet

## Prérequis

Avant de lancer le projet il est necéssaire d'installer certains éléments.

Plusieurs éléments doivent être installés:
- Le client MQTT Paho python doit être installé
- Le broker Mosquitto doit être installé
- Le module Flask doit être installé
- XAMPP doit être installé

Les scripts des clients MQTT utilisent TLS et l'authentification par mot de passe. Ces deux solutions doivent donc être implémentées:
- Implémenter le protocole TLS sur le broker Mosquitto et les client MQTT Phao python
- Implémenter l'authentification par mot de passe entre les clients et le broker

Toutes les installations citées précédemment sont décrites dans la documentation technique (Document_technique.pdf).

## Lancement

**On démarre le serveur xampp:**<br>
*sudo /opt/lampp/lampp start*

**On démarre le broker Mosquitto**<br> 
*sudo mosquitto -v -c '/etc/mosquitto/conf.d/mosquitto.conf'* 

**On démarre le client MQTT 1 (chargé des publications)**<br>
*sudo python '/home/tafonso/paho-mqtt/paho.mqtt.python/src/CliPubTLS.py'* 

**On démarre le client MQTT 2 (chargé de s'abonner)**<br>
*sudo python '/home/tafonso/paho-mqtt/paho.mqtt.python/src/CliSubTLS.py'* 

**On peut désormais se rendre à l'adresse suivante et remplir le formulaire:**<br>
*http://localhost/formul.php*

**Si la réservation a été prise en compte, elle doit être visible dans le tableau des réservations**<br>
*http://localhost/reservation.php*

